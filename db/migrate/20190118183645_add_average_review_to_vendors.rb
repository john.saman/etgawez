class AddAverageReviewToVendors < ActiveRecord::Migration[5.1]
  def change
    add_column :vendors, :average_review, :float
  end
end
