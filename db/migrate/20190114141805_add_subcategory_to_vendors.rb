class AddSubcategoryToVendors < ActiveRecord::Migration[5.1]
  def change
    add_reference :vendors, :subcategory, foreign_key: true, null: true
  end
end
