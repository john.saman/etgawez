class CreateReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :reviews do |t|
      t.references :user, foreign_key: true
      t.references :vendor, foreign_key: true
      t.integer :review
      t.string :comment

      t.timestamps
    end
  end
end
