class CreateVendors < ActiveRecord::Migration[5.1]
  def change
    create_table :vendors do |t|
      t.string :title

      t.string :contact_number
      t.string :address
      t.string :packages
      t.string :fb_page_url
      t.integer :budget

      t.timestamps
    end
  end
end
