class AddReviewsCountToVendors < ActiveRecord::Migration[5.1]
  def change
    add_column :vendors, :reviews_count, :integer
  end
end
