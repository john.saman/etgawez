class CreatePortfolios < ActiveRecord::Migration[5.1]
  def change
    create_table :portfolios do |t|
      t.string :image
      t.references :vendor, foreign_key: true

      t.timestamps
    end
  end
end
