class AddUsernameAndPasswordToVendors < ActiveRecord::Migration[5.1]
  def change
    add_column :vendors, :username, :string
    add_column :vendors, :password_encrypted, :string
    add_column :vendors, :email, :string
  end
end
