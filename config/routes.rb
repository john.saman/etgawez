Rails.application.routes.draw do
  devise_for :admin_users, controllers: { sessions: 'sessions' }
  namespace :admin do
    root to: 'dashboard#index'

    resources :static_pages
    resources :reviews
    resources :users
    resources :portfolios
    resources :videos
    resources :articles
    resources :subcategories
    resources :categories
    resources :vendors
  end

  mount API::Base, at: '/'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
