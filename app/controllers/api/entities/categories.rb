module API
  module Entities
    class Categories < Grape::Entity
      expose :id
      expose :title
      expose :image_url
    end
  end
end
