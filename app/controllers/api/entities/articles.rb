module API
  module Entities
    class Articles < Grape::Entity
      expose :id
      expose :image_url
      expose :title
      expose :content
      expose :created_at
    end
  end
end
