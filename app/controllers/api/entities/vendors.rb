module API
  module Entities
    class Vendors < Grape::Entity
      expose :id
      expose :image_url
      expose :average_review
      expose :reviews_count
    end
  end
end
