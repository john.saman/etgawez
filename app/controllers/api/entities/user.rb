module API
  module Entities
    class User < Grape::Entity
      expose :username
    end
  end
end
