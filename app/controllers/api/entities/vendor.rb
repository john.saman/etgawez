module API
  module Entities
    class Vendor < Grape::Entity
      expose :title
      expose :contact_number
      expose :address
      expose :fb_page_url
      expose :portfolios, using: API::Entities::Portfolio
      expose :reviews, using: API::Entities::Review
      expose :average_review
      expose :reviews_count
    end
  end
end
