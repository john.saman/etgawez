module API
  module Entities
    class Portfolio < Grape::Entity
      expose :image_url
    end
  end
end
