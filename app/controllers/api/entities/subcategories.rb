module API
  module Entities
    class Subcategories < Grape::Entity
      expose :id
      expose :title
      expose :category_id
    end
  end
end
