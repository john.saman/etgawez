module API
  module Entities
    class Videos < Grape::Entity
      expose :id
      expose :video_url
      expose :title
      expose :created_at
    end
  end
end
