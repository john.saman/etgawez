module API
  module Entities
    class Review < Grape::Entity
      expose :review
      expose :user, using: API::Entities::User
      expose :comment
      expose :created_at
    end
  end
end
