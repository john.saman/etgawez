module API
  module V1
    class Reviews < Grape::API
      include API::V1::Defaults

      resource :reviews do
        desc 'Return reviews'
        params do
          requires :vendor_id, type: Integer, desc: 'Vendor ID'
        end
        get '', root: :reviews do
          present Review.where(vendor_id: permitted_params[:vendor_id]), with: API::Entities::Review
        end

        desc 'Record a review'
        params do
          requires :vendor_id, type: Integer, desc: 'Vendor ID'
          requires :review, type: Integer, desc: 'From 1 to 5'
          requires :comment, type: String, desc: 'Review comment'
        end
        post '', root: :reviews do
          authenticate!

          Review.create(user_id: current_user.id ,vendor_id: permitted_params[:vendor_id],
            review: permitted_params[:review], comment: permitted_params[:comment])
        end
      end
    end
  end
end
