module API
  module V1
    class Videos < Grape::API
      include API::V1::Defaults

      resource :videos do
        desc 'Return all videos'
        get '', root: :videos do
          present Video.all, with: API::Entities::Videos
        end
      end
    end
  end
end
