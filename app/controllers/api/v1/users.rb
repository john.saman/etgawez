module API
  module V1
    class Users < Grape::API
      include API::V1::Defaults

      resource :users do
        desc 'sign up'
        params do
          requires :username, type: String, desc: 'Username'
          requires :email, type: String, desc: 'E-mail'
          requires :password, type: String, desc: 'Password'
        end
        post 'sign_up', root: :users do
          User.create(username: permitted_params[:username],
            email: permitted_params[:email], password: permitted_params[:password])
        end

        desc 'change_password'
        params do
          requires :new_password, type: String, desc: 'New Password'
        end
        patch 'change_password', root: :users do
          authenticate!

          current_user.update(password: permitted_params[:new_password])
        end
      end
    end
  end
end
