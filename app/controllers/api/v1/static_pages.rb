module API
  module V1
    class StaticPages < Grape::API
      include API::V1::Defaults

      resource :static_pages do
        desc 'Return a static page'
        params do
          requires :name, type: String, desc: "Name of the
            static page"
        end
        get ':name', root: 'static_pages' do
          StaticPage.where(name: permitted_params[:name]).first!
        end
      end
    end
  end
end
