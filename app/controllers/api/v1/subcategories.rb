module API
  module V1
    class Subcategories < Grape::API
      include API::V1::Defaults

      resource :subcategories do
        desc 'Return subcategories'
        params do
          requires :category_id, type: String, desc: "ID of the
            category"
        end
        get '', root: :subcategories do
          subcategories = Subcategory.where(category_id: permitted_params[:category_id])
          present subcategories, with: API::Entities::Subcategories
        end
      end
    end
  end
end
