module API
  module V1
    class Vendors < Grape::API
      include API::V1::Defaults

      resource :vendors do
        desc 'Search vendors'
        params do
          requires :category_id, type: String, desc: "ID of the
            category"
          optional :subcategory_id, type: String, desc: 'Subcategory ID'
          optional :average_review, type: String
          optional :budget_from, type: Integer
          optional :budget_to, type: Integer
        end
        get '', root: :vendors do
          vendors = Vendor.vendors_with_reviews(category_id: permitted_params[:category_id],
                                                subcategory_id: permitted_params[:subcategory_id], average_review: permitted_params[:average_review],
                                                budget_from: permitted_params[:budget_from], budget_to: permitted_params[:budget_to])
          present vendors, with: API::Entities::Vendors
        end

        desc 'Return a vendor'
        params do
          requires :id, type: String, desc: "ID of the
            vendor"
        end
        get ':id', root: 'vendors' do
          vendor = Vendor.where(id: permitted_params[:id]).eager_load(:portfolios).first!

          present vendor, with: API::Entities::Vendor
        end
      end
    end
  end
end
