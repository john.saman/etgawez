module API
  module V1
    class Articles < Grape::API
      include API::V1::Defaults

      resource :articles do
        desc 'Return all articles'
        get '', root: :articles do
          present Article.all, with: API::Entities::Articles
        end
      end
    end
  end
end
