module API
  module V1
    class Categories < Grape::API
      include API::V1::Defaults

      resource :categories do
        desc 'Return all categories'
        get '', root: :categories do
          present Category.all, with: API::Entities::Categories
        end
      end
    end
  end
end
