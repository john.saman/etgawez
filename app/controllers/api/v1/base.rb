module API
  module V1
    class Base < Grape::API
      mount API::V1::Auth
      mount API::V1::StaticPages
      mount API::V1::Vendors
      mount API::V1::Categories
      mount API::V1::Subcategories
      mount API::V1::Articles
      mount API::V1::Reviews
      mount API::V1::Users
      mount API::V1::Videos
    end
  end
end
