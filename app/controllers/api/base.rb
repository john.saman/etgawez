module API
  class Base < Grape::API
    mount API::V1::Base
    helpers API::Helpers::AuthHelper

    add_swagger_documentation
  end
end
