module Admin
  class AdminUsersController < AdminController
    # GET /admin_users/1/edit
    def edit; end

    # PATCH/PUT /admin_users/1
    # PATCH/PUT /admin_users/1.json
    def update
      respond_to do |format|
        if current_admin_user.update(admin_user_params)
          format.html { redirect_to admin_admin_user_path(current_admin_user), notice: 'Review was successfully updated.' }
          format.json { render :show, status: :ok, location: current_admin_user }
        else
          format.html { render :edit }
          format.json { render json: current_admin_user.errors, status: :unprocessable_entity }
        end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_user_params
      params.require(:admin_user).permit(:email, :password)
    end
  end
end
