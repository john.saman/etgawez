require 'uri'

module ApplicationHelper
  def is_active(page_name)
    uri = URI(request.original_url)
    page_name == uri.path.split('/')[2]
  end
end
