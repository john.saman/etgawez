class Video < ApplicationRecord

  validates :title, presence: true, length: { maximum: 255 }
  validates :content, presence: true
  validates :video, presence: true

  mount_uploader :video, VideoUploader
end
