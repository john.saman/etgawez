class Article < ApplicationRecord

  validates :title, presence: true, length: { maximum: 255 }
  validates :content, presence: true
  validates :image, presence: true

  mount_uploader :image, ArticleImageUploader

  default_scope -> { order(:id) }

end
