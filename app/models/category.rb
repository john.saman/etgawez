class Category < ApplicationRecord
  has_many :subcategories
  has_many :vendors

  validates :title, presence: true, length: { maximum: 255 }
  validates :image, presence: true

  mount_uploader :image, CategoryImageUploader
end
