class Portfolio < ApplicationRecord
  belongs_to :vendor

  validates :vendor_id, presence: true
  validates :image, presence: true

  mount_uploader :image, PortfolioImageUploader
end
