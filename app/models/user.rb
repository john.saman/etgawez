require 'bcrypt'

class User < ApplicationRecord
  has_secure_password

  validates :username,
    presence: true,
    length: { maximum: 100 },
    uniqueness: true

  validates :email,
    presence: true,
    length: { maximum: 255 },
    uniqueness: true
end
