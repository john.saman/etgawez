class Vendor < ApplicationRecord
  belongs_to :category, required: false
  belongs_to :subcategory, required: false
  has_many :portfolios
  has_many :reviews

  validates :title, presence: true, length: { maximum: 100 }
  validates :contact_number, presence: true, length: { maximum: 30 }
  validates :address, presence: true, length: { maximum: 255 }
  validates :fb_page_url, presence: true, length: { maximum: 255 }
  validates :budget, presence: true, numericality: { only_integer: true }
  validates :category_id, presence: true
  validates :image, presence: true

  mount_uploader :image, VendorImageUploader

  scope :vendors_with_reviews, lambda { |category_id: nil, subcategory_id: nil, average_review: nil, budget_from: nil, budget_to: nil|
    query = all
    query = query.where(category_id: category_id) unless category_id.nil?
    query = query.where(subcategory_id: subcategory_id) unless subcategory_id.nil?
    unless average_review.nil?
     query = query.where("average_review >= #{average_review}")
    end
    query = query.where("budget >= #{budget_from}") unless budget_from.nil?
    query = query.where("budget <= #{budget_to}") unless budget_to.nil?

    query
  }
end
