class Review < ApplicationRecord
  belongs_to :user, required: false
  belongs_to :vendor, required: false

  validates :user_id, presence: true
  validates :vendor_id, presence: true
  validates :review, presence: true,
    numericality: { only_integer: true, less_than_or_equal_to: 5, greater_than_or_equal_to: 1 }

  after_save :update_vendor

  def update_vendor
    vendor = Vendor.select('avg(reviews.review) AS average_review, count(reviews.id) AS reviews_count')
            .joins('LEFT JOIN reviews ON vendors.id = reviews.vendor_id')
            .where("vendors.id = #{vendor_id}").first

    self.vendor.update!(average_review: vendor[:average_review], reviews_count: vendor[:reviews_count])
  end
end
