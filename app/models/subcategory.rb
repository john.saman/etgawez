class Subcategory < ApplicationRecord
  belongs_to :category, required: false
  has_many :vendors

  validates :title, presence: true, length: { maximum: 255 }
  validates :category_id, presence: true
end
