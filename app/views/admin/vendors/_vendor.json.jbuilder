json.extract! vendor, :id, :name, :price, :created_at, :updated_at
json.url vendor_url(vendor, format: :json)
