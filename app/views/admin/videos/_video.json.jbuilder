json.extract! video, :id, :title, :content, :video, :created_at, :updated_at
json.url video_url(video, format: :json)
