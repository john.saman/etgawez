json.extract! portfolio, :id, :image, :vendor_id, :created_at, :updated_at
json.url portfolio_url(portfolio, format: :json)
