FactoryBot.define do
  factory :vendor do
    title { 'Title' }
    contact_number { '023143144' }
    address { 'address' }
    fb_page_url { 'http://www.example.com' }
    budget { 10_000 }
    category_id { create(:category).id }
    subcategory_id { create(:subcategory, category_id: category_id).id }
    image { Rack::Test::UploadedFile.new(Rails.root.join('spec/files/test.jpg')) }
  end
end
