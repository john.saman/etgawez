FactoryBot.define do
  factory :category do
    title { 'Title' }
    image { Rack::Test::UploadedFile.new(Rails.root.join('spec/files/test.jpg')) }
  end
end
