FactoryBot.define do
  factory :article do
    title { 'Title' }
    content { 'Content' }
    image { Rack::Test::UploadedFile.new(Rails.root.join('spec/files/test.jpg')) }
  end
end
