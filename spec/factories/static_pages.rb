FactoryBot.define do
  factory :static_page do
    name { 'About' }
    content { 'Content' }
  end
end
