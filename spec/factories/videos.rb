FactoryBot.define do
  factory :video do
    title { 'Title' }
    content { 'Content' }
    video { Rack::Test::UploadedFile.new(Rails.root.join('spec/files/test.mp4')) }
  end
end
