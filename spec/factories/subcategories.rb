FactoryBot.define do
  factory :subcategory do
    title { 'Title' }
    category_id { create(:category).id }
  end
end
