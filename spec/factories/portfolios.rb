FactoryBot.define do
  factory :portfolio do
    vendor_id { create(:vendor).id }
    image { Rack::Test::UploadedFile.new(Rails.root.join('spec/files/test.jpg')) }
  end
end
