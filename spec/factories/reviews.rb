FactoryBot.define do
  factory :review do
    user_id { create(:user).id }
    vendor_id { create(:vendor).id }
    review { 3.0 }
    comment { 'review comment' }
  end
end
