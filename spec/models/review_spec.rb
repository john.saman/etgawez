require 'rails_helper'

describe Review do
  context 'db' do
    context 'indexes' do
      it { should have_db_index(:user_id) }
      it { should have_db_index(:vendor_id) }
    end

    context 'columns' do
      it { is_expected.to have_db_column(:user_id).of_type(:integer) }
      it { is_expected.to have_db_column(:vendor_id).of_type(:integer) }
      it { is_expected.to have_db_column(:review).of_type(:integer) }
      it { is_expected.to have_db_column(:comment).of_type(:string) }
    end
  end

  context 'is valid with valid attributes' do
    subject { build(:review) }
    it { is_expected.to be_valid }
  end
  context 'validations' do
    subject { Review.new }

    it { is_expected.to validate_presence_of :user_id }
    it { is_expected.to validate_presence_of :vendor_id }
    it { is_expected.to validate_numericality_of(:review).only_integer }
  end

  context 'after save' do
    let!(:review) { create(:review) }

    context 'one review' do
      it { expect(review.vendor.average_review).to eq 3.0 }
      it { expect(review.vendor.reviews_count).to eq 1 }
    end

    context 'two reviews' do
      let!(:review2) { create(:review, vendor: review.vendor, review: 4.0) }

      it { expect(review.vendor.average_review).to eq 3.5 }
      it { expect(review.vendor.reviews_count).to eq 2 }
    end

    context 'two reviews with different vendor' do
      let!(:review2) { create(:review, review: 4.0) }

      it { expect(review.vendor.average_review).to eq 3.0 }
      it { expect(review.vendor.reviews_count).to eq 1 }
    end
  end
end
