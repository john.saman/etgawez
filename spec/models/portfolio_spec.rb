require 'rails_helper'

describe Portfolio do
  context 'db' do
    context 'indexes' do
      it { is_expected.to have_db_index(:vendor_id) }
    end

    context 'columns' do
      it { is_expected.to have_db_column(:vendor_id).of_type(:integer) }
      it { is_expected.to have_db_column(:image).of_type(:string) }
    end
  end

  context 'is valid with valid attributes' do
    subject { build(:portfolio) }
    it { is_expected.to be_valid }
  end
  context 'validations' do
    subject { Portfolio.new }

    it { is_expected.to validate_presence_of :vendor_id }
    it { is_expected.to validate_presence_of :image }
  end
end
