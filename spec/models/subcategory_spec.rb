require 'rails_helper'

describe Subcategory do
  context 'db' do
    context 'indexes' do
    end

    context 'columns' do
      it { is_expected.to have_db_column(:title).of_type(:string) }
      it { is_expected.to have_db_column(:category_id).of_type(:integer) }
    end
  end

  context 'is valid with valid attributes' do
    subject { build(:subcategory) }
    it { is_expected.to be_valid }
  end
  context 'validations' do
    subject { Subcategory.new }

    it { is_expected.to validate_presence_of :title }
    it { is_expected.to validate_presence_of :category_id }
    it { is_expected.to validate_length_of(:title).is_at_most(255) }
  end
end
