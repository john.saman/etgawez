require 'rails_helper'

describe Video do
  context 'db' do

    context 'columns' do
      it { is_expected.to have_db_column(:title).of_type(:string) }
      it { is_expected.to have_db_column(:video).of_type(:string) }
    end
  end

  context 'is valid with valid attributes' do
    subject { build(:video) }
    it { is_expected.to be_valid }
  end
  context 'validations' do
    subject { Video.new }

    it { is_expected.to validate_presence_of :title }
    it { is_expected.to validate_presence_of :video }
    it { is_expected.to validate_length_of(:title).is_at_most(255) }
  end
end
