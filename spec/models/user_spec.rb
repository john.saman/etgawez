require 'rails_helper'

describe User do
  context 'db' do
    context 'indexes' do
    end

    context 'columns' do
      it { is_expected.to have_db_column(:username).of_type(:string) }
      it { is_expected.to have_db_column(:password_digest).of_type(:string) }
      it { is_expected.to have_db_column(:email).of_type(:string) }

    end
  end

  context 'is valid with valid attributes' do
    subject { build(:user) }
    it { is_expected.to be_valid }
  end
  context 'validations' do
    subject { build(:user) }

    it { is_expected.to validate_presence_of :username }
    it { is_expected.to validate_length_of(:username).is_at_most(100) }
    it { is_expected.to validate_presence_of :email }
    it { is_expected.to validate_length_of(:email).is_at_most(255) }

    it { is_expected.to validate_uniqueness_of(:username) } # true
    it { is_expected.to validate_uniqueness_of(:email) } # true

  end

  context 'authentication' do
    let(:user) { create(:user, password: 'password') }

    it { expect(user.authenticate('password')).not_to eq false }
    it { expect(user.authenticate('otherpassword')).to eq false }
  end

  context 'change password' do
    let(:user) { create(:user, password: 'password') }

    before {
      user.update(password: 'newpassword')
    }
    it { expect(user.authenticate('newpassword')).not_to eq false }
  end

  context 'do not change password' do
    let(:user) { create(:user, password: 'password') }

    before {
      user.update(username: 'newusername')
    }
    it { expect(user.authenticate('password')).not_to eq false }
  end
end
