require 'rails_helper'

describe StaticPage do
  context 'db' do
    context 'indexes' do
      it { is_expected.to have_db_index(:name) }
    end

    context 'columns' do
      it { is_expected.to have_db_column(:name).of_type(:string) }
      it { is_expected.to have_db_column(:content).of_type(:text) }
    end
  end

  context 'is valid with valid attributes' do
    subject { build(:static_page) }
    it { is_expected.to be_valid }
  end
  context 'validations' do
    subject { StaticPage.new }

    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_length_of(:name).is_at_most(100) }
  end
end
