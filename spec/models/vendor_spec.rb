require 'rails_helper'

describe Vendor do
  context 'db' do
    context 'indexes' do
      it { should have_db_index(:category_id) }
      it { should have_db_index(:subcategory_id) }
    end

    context 'columns' do
      it { is_expected.to have_db_column(:title).of_type(:string) }
      it { is_expected.to have_db_column(:contact_number).of_type(:string) }
      it { is_expected.to have_db_column(:address).of_type(:string) }
      it { is_expected.to have_db_column(:fb_page_url).of_type(:string) }
      it { is_expected.to have_db_column(:budget).of_type(:integer) }
      it { is_expected.to have_db_column(:category_id).of_type(:integer) }
      it { is_expected.to have_db_column(:subcategory_id).of_type(:integer) }
      it { is_expected.to have_db_column(:image).of_type(:string) }
    end
  end

  context 'is valid with valid attributes' do
    subject { build(:vendor) }
    it { is_expected.to be_valid }
  end
  context 'validations' do
    subject { Vendor.new }

    it { is_expected.to validate_presence_of :title }
    it { is_expected.to validate_length_of(:title).is_at_most(100) }
    it { is_expected.to validate_presence_of :contact_number }
    it { is_expected.to validate_length_of(:contact_number).is_at_most(30) }
    it { is_expected.to validate_presence_of :address }
    it { is_expected.to validate_length_of(:address).is_at_most(255) }
    it { is_expected.to validate_presence_of :fb_page_url }
    it { is_expected.to validate_length_of(:fb_page_url).is_at_most(255) }
    it { is_expected.to validate_presence_of :budget }
    it { is_expected.to validate_numericality_of(:budget).only_integer }
    it { is_expected.to validate_presence_of :category_id }
    it { is_expected.not_to validate_presence_of :subcategory_id }
    it { is_expected.to validate_presence_of :image }
  end
end
