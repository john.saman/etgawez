require 'rails_helper'

RSpec.describe API::V1::Auth, type: 'request' do
  describe "GET #index" do
    let!(:user) { create(:user, username: :username) }

    let(:valid_params) { { login: 'username', password: 'password' } }
    let(:invalid_params) { { login: 'username', password: 'invalid_password' } }

    context 'valid login' do
      before do
        post '/api/v1/auth/login', params: valid_params
      end
      it "returns http success" do
        expect(response).to have_http_status(:success)
      end
      it "JSON body response contains token" do
        json_response = JSON.parse(response.body)

        expect(json_response['token']).not_to eq nil
        expect(ApiKey.count).to eq 1
      end
    end

    context 'invalid login' do
      before do
        post '/api/v1/auth/login', params: invalid_params
      end
      it "returns http access denied" do
        expect(response).to have_http_status(401)
      end
      it "JSON body response with nil value" do
        json_response = JSON.parse(response.body)

        expect(json_response['token']).to eq nil
        expect(ApiKey.count).to eq 0
      end
    end
  end
end
