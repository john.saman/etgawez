require 'rails_helper'

RSpec.describe API::V1::Articles, type: 'request' do
  describe "GET #index" do
    let!(:article) { create(:article, created_at: "2019-01-22T10:21:30.628Z") }
    let!(:article2) { create(:article, title: 'Title2', created_at: "2019-01-22T10:21:30.628Z") }

    before do
      get '/api/v1/articles'
    end
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
    it "JSON body response contains expected recipe attributes" do
      json_response = JSON.parse(response.body)

      expect(json_response[0]['title']).to eq "Title"
      expect(json_response[0]['content']).to eq "Content"
      expect(json_response[0]['image_url']).to eq "/uploads/article/image/1/test.jpg"
      expect(json_response[0]['created_at']).to eq "2019-01-22T10:21:30.628Z"

      expect(json_response[1]['title']).to eq "Title2"

    end
  end
end
