require 'rails_helper'

RSpec.describe API::V1::Users, type: 'request' do

  describe 'POST #sign_up' do
    before do
      post '/api/v1/users/sign_up', params: { username: 'username', password: 'password', email: 'test@example.com' }
    end
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
    it "insert review record" do
      expect(User.count).to eq 1
      expect(User.first).to have_attributes({ username: 'username', email: 'test@example.com' })
      expect(User.first.authenticate('password')).not_to eq false
    end
  end

  describe 'PATCH #change_password' do
    let!(:user) { create(:user, username: :username) }

    context 'pass correct password' do
      before do
        post '/api/v1/auth/login', params: { login: 'username', password: 'password' }
        patch '/api/v1/users/change_password', params: { token: JSON.parse(response.body)['token'], new_password: 'newpassword' }
      end
      it "returns http success" do
        expect(response).to have_http_status(:success)
      end
      it "update password" do
        expect(User.first.authenticate('newpassword')).not_to eq false
      end
    end

    context 'pass wrong password' do
      before do
        post '/api/v1/auth/login', params: { login: 'username', password: 'wrongpassword' }
        patch '/api/v1/users/change_password', params: { token: JSON.parse(response.body)['token'], new_password: 'newpassword' }
      end
      it "returns http access denied" do
        expect(response).to have_http_status(401)
      end
      it "update password" do
        expect(User.first.authenticate('newpassword')).to eq false
      end
    end
  end
end
