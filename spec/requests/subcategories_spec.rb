require 'rails_helper'

RSpec.describe API::V1::Subcategories, type: 'request' do
  describe "GET #index" do
    let!(:category) { create(:category) }
    let!(:subcategory) { create(:subcategory, category: category) }
    let!(:subcategory2) { create(:subcategory, title: 'Title2', category: category) }

    let!(:category2) { create(:category) }
    let!(:subcategory3) { create(:subcategory, title: 'Title3', category: category2) }
    let!(:subcategory4) { create(:subcategory, title: 'Title4', category: category2) }

    before do
      get '/api/v1/subcategories', params: { category_id: category.id }
    end
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
    it "JSON body response contains expected data" do
      json_response = JSON.parse(response.body)

      expect(json_response[0]['title']).to eq "Title"
      expect(json_response[0]['category_id']).to eq category.id

      expect(json_response[1]['title']).to eq "Title2"

    end
  end
end
