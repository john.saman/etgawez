require 'rails_helper'

RSpec.describe API::V1::Reviews, type: 'request' do
  describe "GET #index" do
    let!(:vendor) { create(:vendor) }
    let!(:review) { create(:review, vendor: vendor) }
    let!(:review2) { create(:review, vendor: vendor, comment: 'Comment2') }

    let!(:vendor2) { create(:vendor) }
    let!(:review3) { create(:review, vendor: vendor2) }
    let!(:review4) { create(:review, vendor: vendor2) }

    before do
      get '/api/v1/reviews', params: { vendor_id: vendor.id }
    end
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
    it "JSON body response contains expected data" do
      json_response = JSON.parse(response.body)

      expect(json_response[0]['comment']).to eq "review comment"
      expect(json_response[0]['review']).to eq 3.0

      expect(json_response[1]['comment']).to eq "Comment2"

    end
  end

  describe 'POST #create' do
    let!(:vendor) { create(:vendor) }
    let!(:user) { create(:user, username: :username) }

    before do
      post '/api/v1/auth/login', params: { login: 'username', password: 'password' }
      post '/api/v1/reviews', params: { token: JSON.parse(response.body)['token'], vendor_id: vendor.id, review: 3.0, comment: 'review comment' }
    end
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
    it "insert review record" do
      expect(Review.count).to eq 1
      expect(Review.first).to have_attributes({ vendor_id: vendor.id, review: 3.0, comment: 'review comment' })
    end
  end
end
