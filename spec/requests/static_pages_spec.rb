require 'rails_helper'

RSpec.describe API::V1::StaticPages, type: 'request' do
  describe "GET #index" do
    let!(:static_page) { create(:static_page, name: 'About') }
    let!(:static_page2) { create(:static_page, name: 'Biography') }

    before do
      get '/api/v1/static_pages/About'
    end
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
    it "JSON body response contains expected recipe attributes" do
      json_response = JSON.parse(response.body)

      expect(json_response['name']).to eq "About"
      expect(json_response['content']).to eq "Content"
    end
  end
end
