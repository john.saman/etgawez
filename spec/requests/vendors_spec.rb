require 'rails_helper'

RSpec.describe API::V1::Vendors, type: 'request' do
  describe "GET #index" do
    let!(:category) { create(:category) }
    let!(:subcategory) { create(:subcategory, category: category) }

    let!(:vendor) { create(:vendor, category: category) }
    let!(:vendor2) { create(:vendor, category: category, subcategory: subcategory) }
    let!(:vendor3) { create(:vendor, category: category, subcategory: subcategory, average_review: 4.0) }
    let!(:vendor4) { create(:vendor, category: category, subcategory: subcategory, average_review: 4.0, budget: 110000) }

    context 'filter by category' do
      before do
        get '/api/v1/vendors', params: { category_id: category.id }
      end
      it "returns http success" do
        expect(response).to have_http_status(:success)
      end
      it "JSON body response contains expected data" do
        json_response = JSON.parse(response.body)

        expect(json_response.size).to eq 4

        expect(json_response[0]['id']).to eq vendor.id
        expect(json_response[0]['image_url']).to eq vendor.image_url
        expect(json_response[0]['average_review']).to eq vendor.average_review
        expect(json_response[0]['reviews_count']).to eq vendor.reviews_count

        expect(json_response[1]['id']).to eq vendor2.id
      end
    end
    context 'filter by category and subcategory' do
      before do
        get '/api/v1/vendors', params: { category_id: category.id, subcategory_id: subcategory.id }
      end
      it "returns http success" do
        expect(response).to have_http_status(:success)
      end
      it "JSON body response contains expected data" do
        json_response = JSON.parse(response.body)

        expect(json_response.size).to eq 3

        expect(json_response[0]['id']).to eq vendor2.id
        expect(json_response[0]['image_url']).to eq vendor2.image_url
        expect(json_response[0]['average_review']).to eq vendor2.average_review
        expect(json_response[0]['reviews_count']).to eq vendor2.reviews_count

        expect(json_response[1]['id']).to eq vendor3.id
      end
    end

    context 'filter by category and subcategory and average_review' do
      before do
        get '/api/v1/vendors', params: { category_id: category.id, subcategory_id: subcategory.id, average_review: 4.0 }
      end
      it "returns http success" do
        expect(response).to have_http_status(:success)
      end
      it "JSON body response contains expected data" do
        json_response = JSON.parse(response.body)

        expect(json_response.size).to eq 2

        expect(json_response[0]['id']).to eq vendor3.id
        expect(json_response[0]['image_url']).to eq vendor3.image_url
        expect(json_response[0]['average_review']).to eq vendor3.average_review
        expect(json_response[0]['reviews_count']).to eq vendor3.reviews_count

        expect(json_response[1]['id']).to eq vendor4.id
      end
    end

    context 'filter by category and subcategory and average_review and budget' do
      before do
        get '/api/v1/vendors', params: { category_id: category.id, subcategory_id: subcategory.id,
          average_review: 4.0, budget_from: 110000, budget_to: 110000 }
      end
      it "returns http success" do
        expect(response).to have_http_status(:success)
      end
      it "JSON body response contains expected data" do
        json_response = JSON.parse(response.body)

        expect(json_response.size).to eq 1

        expect(json_response[0]['id']).to eq vendor4.id
        expect(json_response[0]['image_url']).to eq vendor4.image_url
        expect(json_response[0]['average_review']).to eq vendor4.average_review
        expect(json_response[0]['reviews_count']).to eq vendor4.reviews_count
      end
    end
  end
end
