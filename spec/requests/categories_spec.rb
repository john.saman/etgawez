require 'rails_helper'

RSpec.describe API::V1::Categories, type: 'request' do
  describe "GET #index" do
    let!(:category) { create(:category, created_at: "2019-01-22T10:21:30.628Z") }
    let!(:category2) { create(:category, title: 'Title2', created_at: "2019-01-22T10:21:30.628Z") }

    before do
      get '/api/v1/categories'
    end
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
    it "JSON body response contains expected data" do
      json_response = JSON.parse(response.body)

      expect(json_response[0]['title']).to eq "Title"
      expect(json_response[0]['image_url']).to eq "/uploads/category/image/1/test.jpg"

      expect(json_response[1]['title']).to eq "Title2"

    end
  end
end
