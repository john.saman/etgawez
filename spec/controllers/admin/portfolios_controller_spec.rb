require 'rails_helper'

describe Admin::PortfoliosController, :type => :controller do

  login_admin_user

  describe "GET #index" do
    let!(:portfolio) { create(:portfolio) }

    it "populates an array of portfolios" do
      get :index
      expect(assigns(:portfolios)).to eq([portfolio])
    end

    it "renders the :index view" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET #show" do
    let!(:portfolio) { create(:portfolio) }

    it "assigns the requested portfolio to @portfolio" do
      get :show, params: { id: portfolio.id }
      expect(assigns(:portfolio)).to eq(portfolio)
    end

    it "renders the #show view" do
      get :show, params: { id: portfolio.id }
      expect(response).to render_template :show
    end
  end

  describe "GET #new" do
    it "display new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    let!(:vendor) { create(:vendor) }
    let(:valid_params) { { portfolio: { image: Rack::Test::UploadedFile.new('spec/files/test.jpg'), vendor_id: vendor.id } } }

    it "create new portfolio" do
      post :create, params: valid_params
      expect(response).to redirect_to admin_portfolio_url(Portfolio.first.id)
      expect(Portfolio.first).to have_attributes valid_params[:portfolio].except(:image)
    end
  end

  describe "GET #edit" do
    let!(:portfolio) { create(:portfolio) }

    it "display edit template" do
      get :edit, params: { id: portfolio.id }
      expect(response).to render_template :edit
    end
  end

  describe "POST #update" do
    let!(:vendor) { create(:vendor) }
    let!(:portfolio) { create(:portfolio) }
    let(:valid_params) { { id: portfolio.id, portfolio: { image: Rack::Test::UploadedFile.new('spec/files/test.jpg'), vendor_id: vendor.id } } }

    it "update portfolio information" do
      patch :update, params: valid_params
      expect(response).to redirect_to admin_portfolio_url(Portfolio.first.id)
      expect(Portfolio.first).to have_attributes valid_params[:portfolio].except(:image)
    end
  end
end
