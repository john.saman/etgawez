require 'rails_helper'

describe Admin::CategoriesController, :type => :controller do

  login_admin_user

  describe "GET #index" do
    let!(:category) { create(:category) }

    it "populates an array of categories" do
      get :index
      expect(assigns(:categories)).to eq([category])
    end

    it "renders the :index view" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET #show" do
    let!(:category) { create(:category) }

    it "assigns the requested contact to @category" do
      get :show, params: { id: category.id }
      expect(assigns(:category)).to eq(category)
    end

    it "renders the #show view" do
      get :show, params: { id: category.id }
      expect(response).to render_template :show
    end
  end

  describe "GET #new" do
    it "display new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    let(:valid_params) { { category: { title: 'Title', image: Rack::Test::UploadedFile.new('spec/files/test.jpg') } } }

    it "create new category" do
      post :create, params: valid_params
      expect(response).to redirect_to admin_category_url(Category.first.id)
      expect(Category.first).to have_attributes valid_params[:category].except(:image)
    end
  end

  describe "GET #edit" do
    let!(:category) { create(:category) }

    it "display edit template" do
      get :edit, params: { id: category.id }
      expect(response).to render_template :edit
    end
  end

  describe "POST #update" do
    let!(:category) { create(:category) }
    let(:valid_params) { { id: category.id, category: { title: 'New Title', image: Rack::Test::UploadedFile.new('spec/files/test.jpg') } } }

    it "update category information" do
      patch :update, params: valid_params
      expect(response).to redirect_to admin_category_url(Category.first.id)
      expect(Category.first).to have_attributes valid_params[:category].except(:image)
    end
  end
end
