require 'rails_helper'

describe Admin::VideosController, :type => :controller do

  login_admin_user

  describe "GET #index" do
    let!(:video) { create(:video) }

    it "populates an array of videos" do
      get :index
      expect(assigns(:videos)).to eq([video])
    end

    it "renders the :index view" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET #show" do
    let!(:video) { create(:video) }

    it "assigns the requested contact to @video" do
      get :show, params: { id: video.id }
      expect(assigns(:video)).to eq(video)
    end

    it "renders the #show view" do
      get :show, params: { id: video.id }
      expect(response).to render_template :show
    end
  end

  describe "GET #new" do
    it "display new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    let(:valid_params) { { video: { title: 'Title', content: 'Content', video: Rack::Test::UploadedFile.new('spec/files/test.mp4') } } }

    it "create new video" do
      post :create, params: valid_params
      expect(response).to redirect_to admin_video_url(Video.first.id)
      expect(Video.first).to have_attributes valid_params[:video].except(:video)
    end
  end

  describe "GET #edit" do
    let!(:video) { create(:video) }

    it "display edit template" do
      get :edit, params: { id: video.id }
      expect(response).to render_template :edit
    end
  end

  describe "POST #update" do
    let!(:video) { create(:video) }
    let(:valid_params) { { id: video.id, video: { title: 'New Title', content: 'New content', video: Rack::Test::UploadedFile.new('spec/files/test.mp4') } } }

    it "update video information" do
      patch :update, params: valid_params
      expect(response).to redirect_to admin_video_url(Video.first.id)
      expect(Video.first).to have_attributes valid_params[:video].except(:video)
    end
  end
end
