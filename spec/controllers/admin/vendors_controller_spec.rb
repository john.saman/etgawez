require 'rails_helper'

describe Admin::VendorsController, :type => :controller do

  login_admin_user

  describe "GET #index" do
    let!(:vendor) { create(:vendor) }

    it "populates an array of vendors" do
      get :index
      expect(assigns(:vendors)).to eq([vendor])
    end

    it "renders the :index view" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET #show" do
    let!(:vendor) { create(:vendor) }

    it "assigns the requested contact to @vendor" do
      get :show, params: { id: vendor.id }
      expect(assigns(:vendor)).to eq(vendor)
    end

    it "renders the #show view" do
      get :show, params: { id: vendor.id }
      expect(response).to render_template :show
    end
  end

  describe "GET #new" do
    it "display new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    let!(:category) { create(:category) }
    let!(:subcategory) { create(:subcategory, category: category) }

    let(:valid_params) { { vendor: {
      title: 'Title',
      category_id: category.id,
      subcategory_id: subcategory.id,
      image: Rack::Test::UploadedFile.new('spec/files/test.jpg'),
      contact_number: '0123456789',
      budget: 100000,
      address: 'Address',
      fb_page_url: 'http://www.facebook.com'
    } } }

    it "create new vendor" do
      post :create, params: valid_params
      expect(response).to redirect_to admin_vendor_url(Vendor.first.id)
      expect(Vendor.first).to have_attributes valid_params[:vendor].except(:image)
    end
  end

  describe "GET #edit" do
    let!(:vendor) { create(:vendor) }

    it "display edit template" do
      get :edit, params: { id: vendor.id }
      expect(response).to render_template :edit
    end
  end

  describe "POST #update" do
    let!(:category) { create(:category) }
    let!(:subcategory) { create(:subcategory, category: category) }

    let!(:vendor) { create(:vendor) }
    let(:valid_params) { {  id: vendor.id, vendor: {
      title: 'Title',
      category_id: category.id,
      subcategory_id: subcategory.id,
      image: Rack::Test::UploadedFile.new('spec/files/test.jpg'),
      contact_number: '0123456789',
      budget: 100000,
      address: 'Address',
      fb_page_url: 'http://www.facebook.com'
    } } }

    it "update vendor information" do
      patch :update, params: valid_params
      expect(response).to redirect_to admin_vendor_url(Vendor.first.id)
      expect(Vendor.first).to have_attributes valid_params[:vendor].except(:image)
    end
  end
end
