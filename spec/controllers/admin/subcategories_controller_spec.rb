require 'rails_helper'

describe Admin::SubcategoriesController, :type => :controller do

  login_admin_user

  describe "GET #index" do
    let!(:subcategory) { create(:subcategory) }

    it "populates an array of subcategories" do
      get :index
      expect(assigns(:subcategories)).to eq([subcategory])
    end

    it "renders the :index view" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET #show" do
    let!(:subcategory) { create(:subcategory) }

    it "assigns the requested subcategory to @subcategory" do
      get :show, params: { id: subcategory.id }
      expect(assigns(:subcategory)).to eq(subcategory)
    end

    it "renders the #show view" do
      get :show, params: { id: subcategory.id }
      expect(response).to render_template :show
    end
  end

  describe "GET #new" do
    it "display new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    let!(:category) { create(:category) }
    let(:valid_params) { { subcategory: { title: 'Title', category_id: category.id } } }

    it "create new subcategory" do
      post :create, params: valid_params
      expect(response).to redirect_to admin_subcategory_url(Subcategory.first.id)
      expect(Subcategory.first).to have_attributes valid_params[:subcategory]
    end
  end

  describe "GET #edit" do
    let!(:subcategory) { create(:subcategory) }

    it "display edit template" do
      get :edit, params: { id: subcategory.id }
      expect(response).to render_template :edit
    end
  end

  describe "POST #update" do
    let!(:category) { create(:category) }
    let!(:subcategory) { create(:subcategory) }
    let(:valid_params) { { id: subcategory.id, subcategory: { title: 'New Title', category_id: category.id } } }

    it "update subcategory information" do
      patch :update, params: valid_params
      expect(response).to redirect_to admin_subcategory_url(Subcategory.first.id)
      expect(Subcategory.first).to have_attributes valid_params[:subcategory]
    end
  end
end
