require 'rails_helper'

describe Admin::UsersController, :type => :controller do

  login_admin_user

  describe "GET #index" do
    let!(:user) { create(:user) }

    it "populates an array of users" do
      get :index
      expect(assigns(:users)).to eq([user])
    end

    it "renders the :index view" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET #show" do
    let!(:user) { create(:user) }

    it "assigns the requested contact to @user" do
      get :show, params: { id: user.id }
      expect(assigns(:user)).to eq(user)
    end

    it "renders the #show view" do
      get :show, params: { id: user.id }
      expect(response).to render_template :show
    end
  end

  describe "GET #new" do
    it "display new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    let(:valid_params) { { user: { username: 'Username', email: 'test@example.com', password: 'password' } } }

    it "create new user" do
      post :create, params: valid_params
      expect(response).to redirect_to admin_user_url(User.first.id)
      expect(User.first).to have_attributes valid_params[:user].except(:password)
      expect(User.first.authenticate('password')).not_to eq false
      expect(User.first.authenticate('invalidpassword')).to eq false
    end
  end

  describe "GET #edit" do
    let!(:user) { create(:user) }

    it "display edit template" do
      get :edit, params: { id: user.id }
      expect(response).to render_template :edit
    end
  end

  describe "POST #update" do
    let!(:user) { create(:user) }
    let(:valid_params) { { id: user.id, user: { username: 'Username', email: 'test@example.com', password: 'password' } } }

    it "update user information" do
      patch :update, params: valid_params
      expect(response).to redirect_to admin_user_url(User.first.id)
      expect(User.first).to have_attributes valid_params[:user].except(:password)
      expect(User.first.authenticate('password')).not_to eq false
      expect(User.first.authenticate('invalidpassword')).to eq false

    end
  end
end
