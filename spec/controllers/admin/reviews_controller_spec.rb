require 'rails_helper'

describe Admin::ReviewsController, :type => :controller do

  login_admin_user

  describe "GET #index" do
    let!(:review) { create(:review) }

    it "populates an array of reviews" do
      get :index
      expect(assigns(:reviews)).to eq([review])
    end

    it "renders the :index view" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET #show" do
    let!(:review) { create(:review) }

    it "assigns the requested review to @review" do
      get :show, params: { id: review.id }
      expect(assigns(:review)).to eq(review)
    end

    it "renders the #show view" do
      get :show, params: { id: review.id }
      expect(response).to render_template :show
    end
  end

  describe "GET #new" do
    it "display new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    let!(:user) { create(:user) }
    let!(:vendor) { create(:vendor) }

    let(:valid_params) { { review: { user_id: user.id, vendor_id: vendor.id, review: 3, comment: 'Comment' } } }

    it "create new review" do
      post :create, params: valid_params
      expect(response).to redirect_to admin_review_url(Review.first.id)
      expect(Review.first).to have_attributes valid_params[:review]
    end
  end

  describe "GET #edit" do
    let!(:review) { create(:review) }

    it "display edit template" do
      get :edit, params: { id: review.id }
      expect(response).to render_template :edit
    end
  end

  describe "POST #update" do
    let!(:user) { create(:user) }
    let!(:vendor) { create(:vendor) }

    let!(:review) { create(:review) }
    let(:valid_params) { { id: review.id, review: { user_id: user.id, vendor_id: vendor.id, review: 3, comment: 'Comment' } } }

    it "update review information" do
      patch :update, params: valid_params
      expect(response).to redirect_to admin_review_url(Review.first.id)
      expect(Review.first).to have_attributes valid_params[:review]
    end
  end
end
