require 'rails_helper'

describe Admin::StaticPagesController, :type => :controller do

  login_admin_user

  describe "GET #index" do
    let!(:static_page) { create(:static_page) }

    it "populates an array of static_pages" do
      get :index
      expect(assigns(:static_pages)).to eq([static_page])
    end

    it "renders the :index view" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET #show" do
    let!(:static_page) { create(:static_page) }

    it "assigns the requested contact to @static_page" do
      get :show, params: { id: static_page.id }
      expect(assigns(:static_page)).to eq(static_page)
    end

    it "renders the #show view" do
      get :show, params: { id: static_page.id }
      expect(response).to render_template :show
    end
  end

  describe "GET #new" do
    it "display new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    let(:valid_params) { { static_page: { name: 'Title', content: 'Content' } } }

    it "create new static_page" do
      post :create, params: valid_params
      expect(response).to redirect_to admin_static_page_url(StaticPage.first.id)
      expect(StaticPage.first).to have_attributes valid_params[:static_page]
    end
  end

  describe "GET #edit" do
    let!(:static_page) { create(:static_page) }

    it "display edit template" do
      get :edit, params: { id: static_page.id }
      expect(response).to render_template :edit
    end
  end

  describe "POST #update" do
    let!(:static_page) { create(:static_page) }
    let(:valid_params) { { id: static_page.id, static_page: { name: 'New Title', content: 'New content' } } }

    it "update static_page information" do
      patch :update, params: valid_params
      expect(response).to redirect_to admin_static_page_url(StaticPage.first.id)
      expect(StaticPage.first).to have_attributes valid_params[:static_page]
    end
  end
end
