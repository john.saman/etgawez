require 'rails_helper'

describe Admin::ArticlesController, :type => :controller do

  login_admin_user

  describe "GET #index" do
    let!(:article) { create(:article) }

    it "populates an array of articles" do
      get :index
      expect(assigns(:articles)).to eq([article])
    end

    it "renders the :index view" do
      get :index
      expect(response).to render_template :index
    end
  end

  describe "GET #show" do
    let!(:article) { create(:article) }

    it "assigns the requested contact to @article" do
      get :show, params: { id: article.id }
      expect(assigns(:article)).to eq(article)
    end

    it "renders the #show view" do
      get :show, params: { id: article.id }
      expect(response).to render_template :show
    end
  end

  describe "GET #new" do
    it "display new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    let(:valid_params) { { article: { title: 'Title', content: 'Content', image: Rack::Test::UploadedFile.new('spec/files/test.jpg') } } }

    it "create new article" do
      post :create, params: valid_params
      expect(response).to redirect_to admin_article_url(Article.first.id)
      expect(Article.first).to have_attributes valid_params[:article].except(:image)
    end
  end

  describe "GET #edit" do
    let!(:article) { create(:article) }

    it "display edit template" do
      get :edit, params: { id: article.id }
      expect(response).to render_template :edit
    end
  end

  describe "POST #update" do
    let!(:article) { create(:article) }
    let(:valid_params) { { id: article.id, article: { title: 'New Title', content: 'New Content', image: Rack::Test::UploadedFile.new('spec/files/test.jpg') } } }

    it "update article" do
      patch :update, params: valid_params
      expect(response).to redirect_to admin_article_url(Article.first.id)
      expect(Article.first).to have_attributes valid_params[:article].except(:image)
    end
  end
end
